#![no_main]
#![no_std]

use cortex_m_rt::entry;
use derive_more::From;
use embedded_hal::{adc::OneShot, prelude::*};
// gpio::p0::Parts as P0Parts,
#[allow(unused_imports)]
use microbit::hal::saadc::{InternalVdd, Saadc, SaadcConfig};
use panic_rtt_target as _;
use rtt_target::{rprintln, rtt_init_print};

#[entry]
fn main() -> ! {
    rtt_init_print!();
    match run() {
        Err(err) => {
            rprintln!("FATAL ERROR: {:?}", err)
        }
        Ok(()) => {
            rprintln!("PROGRAM ENDED")
        }
    }
    #[allow(clippy::empty_loop)]
    loop {}
}

fn run() -> Result<(), Error> {
    let mut board = microbit::Board::take().ok_or(Error::BoardTake)?;
    let mut delay = microbit::hal::delay::Delay::new(board.SYST);
    // Circuit
    //
    // The micro:bit can only measure voltages up to the reference
    // voltage (selected to by 0.6V below).  So, we connect the 5.5V
    // batter in a voltage divider circuit with one 10K ohm and one 1K
    // ohm resistors.
    //
    // The micro:bit is powered by a laptop over USB, but it's capable
    // of measuring the (reduced) voltage of the battery.
    //
    //              laptop
    //                 |
    //                USB
    //             micro:bit
    //             P1     GND
    //             |       |
    //             *--10K--*
    //             |       |
    //             1K      BAT (5.5V)
    //             |       |
    //             *-------*
    let mut saadc = Saadc::new(
        board.SAADC,
        SaadcConfig {
            // "Internal" voltage of 0.6V
            reference: microbit::pac::saadc::ch::config::REFSEL_A::INTERNAL,

            // divide by 1/6 (i.e. multiply by 6)
            gain: microbit::pac::saadc::ch::config::GAIN_A::GAIN1_6,
            // // resistor: microbit::pac::saadc::ch::config::RESP_A::PULLDOWN,

            // 0 is 0, and reference * gain is 2^resolution
            resolution: microbit::pac::saadc::resolution::VAL_A::_14BIT,
            ..Default::default()
        },
    );
    rprintln!("Lock & load!");
    loop {
        // Big pin 1 on the edge connector.
        let p1v = saadc.read(&mut board.pins.p0_03)?;

        // This is all computed based on the info in the nRD52833
        // Datasheet at page 361.
        rprintln!(
            "Voltage: {} {:.2}",
            p1v,
            (p1v as f32) * ((0.6 * 6.0) / ((1 << 14) as f32))
        );
        delay.delay_ms(1000_u16);
    }
}

#[derive(Debug, From)]
enum Error {
    Fmt(core::fmt::Error),
    Rtc(microbit::hal::rtc::Error),
    Nb(nb::Error<()>),
    BoardTake,
}
