{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    naersk = {
      url = "github:nix-community/naersk";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    rust-overlay = {
      url = "github:oxalica/rust-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.flake-utils.follows = "flake-utils";
    };
  };

  outputs = { self, nixpkgs, flake-utils, naersk, rust-overlay, ... }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        overlays = [ (import rust-overlay) ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
        rust = (pkgs.rust-bin.stable.latest.default.override {
          extensions = [ "rust-src" "rust-analyzer" "llvm-tools-preview" ];
          targets = [ "thumbv7em-none-eabihf" ];
        });
        naersk-lib = naersk.lib."${system}";
        libPath = with pkgs; lib.makeLibraryPath [
        ];
      in
      rec {
        # `nix build`
        packages.app = naersk-lib.buildPackage {
          src = ./.;
        };
        defaultPackage = packages.app;

        # `nix run`
        apps.app = flake-utils.lib.mkApp {
          drv = packages.app;
        };
        defaultApp = apps.app;

        # `nix develop`
        devShell = pkgs.mkShell {
          nativeBuildInputs = with pkgs; [
            cargo-binutils
            cargo-embed
            cargo-nextest
            cargo-outdated
            difftastic
            exa
            just
            minicom
            pkg-config
            pre-commit
            rust
            tokei
            usbutils
            pkgsCross.arm-embedded.buildPackages.gdb
          ];

          LD_LIBRARY_PATH = libPath;
          GIT_EXTERNAL_DIFF = "${pkgs.difftastic}/bin/difft";
        };
      }
    );
}
